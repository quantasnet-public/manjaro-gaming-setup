1. Run Manjaro Installer with propietary drivers
2. Maybe don't pick encrypted partition... (slow boot)
3. Disable OpenGL Vsync in `nvidia-settings`, set powermizer to `Prefer Maximum Performance`
4. Enable AUR in the package manager
5. Install Steam (native) from package manager
    - Go to Settings > Interface > Uncheck `Enlarge text and icons based on monitor size`
    - Go to Settings > Steam Play > Enable steam play for all other titles (enables Proton)
7. Display Settings > Set to 120Hz
8. GNOME Extensions - Disable `Dash to Dock`
9. GNOME Extensions - Install (to auto hide top bar):
    - https://extensions.gnome.org/extension/545/hide-top-bar/
10. Mount second drive in /etc/fstab using UUID from gparted (or wherever)
11. Install xow and run xow-get-firmware.sh (https://www.addictivetips.com/ubuntu-linux-tips/wireless-xbox-one-controllers-on-linux-with-xow/)
12. Prepare Wine for Lutris (https://github.com/lutris/docs/blob/master/WineDependencies.md)
13. Install Lutris
14. Install gnome-keyring (for minecraft)
15. Install Discord
14. Install battle.net in Steam
    - Download battle.net installer from blizzard
    - Add to steam as non steam app
    - Set compatibility for Proton
    - Run the installer
    - Remove the installer
    - Add battle.net as non steam game:
    - Navigate to the wineprefix created /home/andrew/.local/share/Steam/compatdata/(folder with newest timestamp probably)

15. Install goverlay, mangohud
16. HiDPI settings (https://wiki.archlinux.org/title/HiDPI#Steam)
17. Turn off night mode
18. Emudeck
    - Run `pacman -S fuse2` - then reboot
    - Install any game in Steam, tell it to use proton 7.0 for compat and start it once
    - Install discover from app store
    - Install EmuDeck (copy the run command from the .desktop file and just run that)
    - Install steam-rom-manager-git from the AUR because the AppImage doesn't start?
    - Install yuzu from Discover store
20. Syncthing
    - `sudo pacman -S syncthing`
    - `sudo loginctl enable-linger andrew`
    - `systemctl enable syncthing.service --user --now`
21. Install vulkan tools
    - `sudo pacman -S --needed nvidia-utils lib32-nvidia-utils vulkan-icd-loader lib32-vulkan-icd-loader`
22. Install DXVK-BIN from AUR
23. Disable sleep/hibernate - `systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target`
Other

1. install docker, docker-compose
2. `systemctl enable docker --now`
3. 
